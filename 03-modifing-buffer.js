const charCodeOfPeriod = 46;

process.stdin.on('data', buffer => {
  for(let i = 0; i < buffer.length; i++) {
    if (buffer[i] === charCodeOfPeriod) {
      buffer[i] = '!'.charCodeAt(0);
    }
  }
  console.log(buffer);
});
