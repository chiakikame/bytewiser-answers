const srcFile = process.argv[2];
const fs = require('fs');
const newLine = '\n'.charCodeAt(0);

let start = 0;
fs.readFile(srcFile, (err, buf) => {
  if (err) return;
  
  for(let i = 0; i < buf.length; i++) {
    if (buf[i] === newLine) {
      console.log(buf.slice(start, i));
      start = i + 1;
    }
  }
  console.log(buf.slice(start));
});
