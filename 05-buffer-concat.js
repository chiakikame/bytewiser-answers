const stream = require('stream');

const concatStream = new stream.Writable({
  write(chunk, enc, next) {
    this.buffers = this.buffers ? this.buffers : [];
    console.error(this.buffers.length);
    this.buffers.push(chunk);
    next();
  }
});

concatStream.on('finish', function() {
  console.log(Buffer.concat(this.buffers));
});

process.stdin.pipe(concatStream);
